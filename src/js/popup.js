(function ($) {
  'use strict'

  // функция открытия окна
  function openPopup(url) {
    $('#popup .overlay').fadeIn(300, function () {
      $('html').css({ 'overflow-y': 'hidden', 'overflow-x': 'hidden' })
      $('#popup, #popup .overlay').show()
      $(url).css('display', 'flex').animate({ opacity: 1 }, 300)
    })
  }

  // функция закрытия окна
  function closePopup() {
    $('#popup .module').animate({ opacity: 0 }, 300, function () {
      history.pushState('', document.title, window.location.pathname)

      $('#popup, #popup .overlay, #popup .module').hide()
      $('#popup .overlay').fadeOut(300)
      $('html').css('overflow-y', 'visible')
    })
  }

  // функция переключения окон
  function togglePopup(url) {
    $('#popup .module').hide()
    $(url).css('display', 'flex').animate({ opacity: 1 }, 300)
  }

  let hashes = ['answer_1', 'answer_2', 'answer_3'],
      url

  // открывает окно
  $('.open-popup').on('click', function (event) {
    event.preventDefault()

    url = $(this).attr('href')

    openPopup(url)
    event.stopPropagation()
  })

  // ..при другом открытом окне
  $('.toggle-popup').on('click', function (event) {
    event.preventDefault()

    url = $(this).attr('href')

    togglePopup(url)
    event.stopPropagation()
  })

  // ..при динамическом изменении страницы
  window.addEventListener('hashchange', function (event) {
    url = decodeURI(window.location.hash)

    $.each(hashes, function (index, value) {
      if (url.replace('#', '') == value.toString()) togglePopup(url)
    })

    event.stopPropagation()
  })

  // закрывает окно
  $('.close-popup').on('click', function (event) {
    event.preventDefault()
    closePopup()
  })

  // ..при нажатии на клавишу Escape
  $('body').on('keydown', function (event) {
    if (event.code == 'Escape') closePopup()
  })
}(jQuery))
