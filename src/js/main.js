(function ($) {
  'use strict'

  // плавная прокрутка к якорю
  $('.hash').on('click', function (event) {
    event.preventDefault()

    let sectionId = $(this).attr('href'),
        pageTop = $(sectionId).offset().top - 70

    $('html, body').animate({ scrollTop: pageTop }, 600)
  })

  // закрывает баннер
  $('.close-banner').on('click', function (event) {
    event.preventDefault()

    $('#banner').hide()
  })
}(jQuery))
