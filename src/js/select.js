(function ($) {
  'use strict'

  // стилизует select
  $(window).one('load', function () {
    let currentSelect = ''

    // функция показа вариантов для выбора
    function openSelect(currentSelect) {
      $('.select-options[data-for="' + currentSelect + '"]').css('display', 'flex')
    }

    // функция скрытия вариантов
    function closeSelect() {
      $('.select-options').hide()
    }

    // показывает варианты при клике на поле
    $('.select-box').on('click', function (event) {
      event.preventDefault()

      currentSelect = $(this).attr('id')

      if ($(this).siblings('.select-options').is(':visible')) {
        closeSelect()
      } else {
        closeSelect()
        openSelect(currentSelect)

        event.stopPropagation()
      }
    })

    // обрабатывает клик по вариантам
    $('.select-options').on('click', '.option', function () {
      // обновляет значение поля select
      $('#' + currentSelect).html($(this).text())
      $('input[name=' + currentSelect + ']').val($(this).attr('data-id'))

      closeSelect()
    })

    // прячет варианты
    $(document).on('click', function (event) {
      if (!$('.option').is(event.target)) {
        closeSelect()
      }
    })
  })
}(jQuery))
