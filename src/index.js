'use strict'

// js
import './js/main'
import './js/popup'
import './js/select'

// css
import './assets/css/main.css'

// scss
import './assets/scss/main.scss'
