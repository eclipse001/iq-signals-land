const webpack = require('webpack')
const path = require('path')
const TerserPlugin = require('terser-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')

const PATHS = {
  src: path.join(__dirname, '../src'),
  view: path.join(__dirname, '../src/view'),
  public: path.join(__dirname, '../public'),
  assets: 'assets',
  image: 'image'
}

module.exports = {
  externals: {
    paths: PATHS
  },
  entry: {
    main: PATHS.src
  },
  output: {
    filename: `${ PATHS.assets }/js/[name].js?v=[contenthash:7]`,
    path: PATHS.public,
    publicPath: '/'
  },
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        extractComments: false
      })
    ],
    splitChunks: {
      cacheGroups: {
        vendor: {
          name: 'vendors',
          test: /node_modules/,
          chunks: 'all',
          enforce: true
        }
      }
    }
  },
  performance: {
    hints: false
  },
  module: {
    rules: [
      {
        test: /\.pug$/i,
        loader: 'pug-loader',
        options: { pretty: true }
      }, {
        test: /\.js$/i,
        loader: 'babel-loader',
        exclude: '/node_modules/'
      }, {
        test: /\.(jpe?g|png|svg)$/i,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: `${ PATHS.image }/[folder]/[name].[ext]`
            }
          }, {
            loader: 'image-webpack-loader',
            options: {
              mozjpeg: { progressive: true, quality: 65 },
              optipng: { enabled: false },
              pngquant: { quality: [0.65, 0.90], speed: 4 },
              gifsicle: { interlaced: false },
              webp: { quality: 100, method: 6, sns: 0, lossless: true }
            }
          }
        ]
      }, {
        test: /\.scss$/i,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: { sourceMap: true }
          }, {
            loader: 'postcss-loader',
            options: { sourceMap: true, postcssOptions: { config: './config/postcss.config.js' } }
          }, {
            loader: 'sass-loader',
            options: { sourceMap: true }
          }
        ]
      }, {
        test: /\.css$/i,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: { sourceMap: true }
          }, {
            loader: 'postcss-loader',
            options: { sourceMap: true, postcssOptions: { config: './config/postcss.config.js' } }
          }
        ]
      }
    ]
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery'
    }),
    new MiniCssExtractPlugin({
      filename: `${ PATHS.assets }/css/[name].css?v=[contenthash:7]`
    }),
    new HtmlWebpackPlugin({
      filename: './index.html', template: `${ PATHS.view }/index.pug`, chunks: ['vendors', 'main'], inject: 'body', minify: false
    }),
    new CopyWebpackPlugin({
      patterns: [
        { from: `${ PATHS.src }/assets/image`, to: PATHS.image },
        { from: `${ PATHS.src }/static`, to: '' }
      ]
    })
  ]
}
