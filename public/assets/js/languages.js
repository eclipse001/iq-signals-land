
let language = [
  { id: 1, url: '/', title: 'English' },
  //{ id: 2, url: '/pt.html', title: 'Português' },
  // { id: 3, url: '#', title: 'हिंदी, ไทย' },
  // { id: 4, url: '#', title: 'Türk' },
  // { id: 5, url: '#', title: 'Indonesia' },
  // { id: 6, url: '#', title: 'Español' },
  // { id: 7, url: '#', title: 'Русский' }
]

let parent = document.querySelector('.select-options')

for (let l in language) {
  const a = document.createElement('a')
  a.href = language[l].url
  a.setAttribute('data-id', language[l].id)
  a.classList.add('option')
  a.setAttribute('title', language[l].title)
  a.innerHTML = language[l].title

  parent.append(a)
}
