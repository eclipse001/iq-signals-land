# IQ Signals change log

## Version 1.0.0 under development

- New: Added jQuery
- New: Added Index page template
- New: Added Roboto font
- New: Added Pug, SCSS, PostCSS
- New: Added Webpack, Babel

## Project init May 21, 2021
